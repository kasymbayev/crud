@extends('layout')

@section('content')

    <div class="container">
        <h3>Create Tasks</h3>
        @include('errors')
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['route' => 'tasks.store']) !!}
                    <div class="form-group">
                        <input type="text" name="title" class="form-control" value="{{old('title')}}">
                        <br>
                        <textarea name="description" class="form-control" cols="30" rows="10">{{old('description')}}</textarea>
                        <br>
                        <button class="btn btn-success">Create</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection