@extends('layout')

@section('content')

    <div class="container">
        <h3>Edit task # - {{$task -> id}}</h3>
        @include('errors')
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['route' => ['tasks.update',$task->id],'method' => 'PUT']) !!}
                <div class="form-group">
                    <input type="text" name="title" class="form-control" value="{{$task->title}}">
                    <br>
                    <textarea name="description" class="form-control" cols="30" rows="10">{{$task->description}}</textarea>
                    <br>
                    <button class="btn btn-warning">Edit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection