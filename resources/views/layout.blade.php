<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Posts</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>
        form{
            display: inline;
        }

        form button{
            background: transparent;
            padding:0;
            border:0;
        }

        form button i{
            color: #337ab7;
        }
    </style>

</head>
<body>
    @yield('content')
</body>
</html>